
    public enum ScreenType
    {
        MainMenu,
        Pause,
        Victory,
        Defeat,
        Shop,
        Confirmation,
        Currency,
        DailyBonus,
        SpecialOffer,
        RateUs,
        LevelChest,
        StarChest,
        InternetFail,
        PurchaseFail,
        Loading,
        LevelView,
        Settings,
        Energy,
        RewardedItems,
        Inventory
    }
