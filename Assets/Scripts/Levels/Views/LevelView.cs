using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class LevelView : BaseView
{
    public static Action<string> ShowTextEventHandler; 

    [Header("Panels")]
    [SerializeField] private RectTransform textPanel;
    [SerializeField] private CanvasGroup textPanelCanvasGroup;

    [Header("Buttons")]
    [SerializeField] private Button pauseButton;
    [SerializeField] private Button hintButton;
    [SerializeField] private Button useButton;
    [SerializeField] private Button inventoryButton;

    [Header("Texts")]
    [SerializeField] private TextMeshProUGUI dialogText;

    private bool isShowingText;

    protected override void Start()
    {
        base.Start();

        pauseButton.onClick.AddListener(PauseButtonPressed);
        hintButton.onClick.AddListener(HintButtonPressed);
        inventoryButton.onClick.AddListener(InventoryButtonPressed);

        ShowTextEventHandler += ShowText;
    }
    private void PauseButtonPressed()
    {
        OpenScreen(ScreenType.Pause);
    }
    private void HintButtonPressed()
    {

    }
    private void InventoryButtonPressed()
    {
        OpenScreen(ScreenType.Inventory);
        CloseScreen(ScreenType.LevelView);
    }
    private void Update()
    {
        if(PlayerController.isNearLeverArm == true)
        {
            useButton.gameObject.SetActive(true);
        }
        else useButton.gameObject.SetActive(false);
    }
    public void ShowText(string text)
    {
        if(isShowingText == false)
        {
            StartCoroutine(_ShowText(text));
        }
    }
   private IEnumerator _ShowText(string text)
    {
        isShowingText = true;
        textPanel.position = GameController.playerController.transform.position + Vector3.up * 1;

        dialogText.text = string.Empty;

        textPanelCanvasGroup.alpha = 0;
        textPanel.gameObject.SetActive(true);
        textPanelCanvasGroup.DOFade(1, 0.1f);
        GameController.playerController.EnablePlayerController(false);
        foreach (char c in text.ToCharArray())
        {
            dialogText.text += c;
            yield return new WaitForSeconds(0.05f);
        }

        yield return new WaitForSeconds(1f);

        GameController.playerController.EnablePlayerController(true); 
        textPanel.gameObject.SetActive(false);
        isShowingText = false;
    }
    private void OnDestroy()
    {
        ShowTextEventHandler = null;
    }
}
