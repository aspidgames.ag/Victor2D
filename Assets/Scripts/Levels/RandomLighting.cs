using System.Collections;
using UnityEngine;
using DG.Tweening;

public class RandomLighting : MonoBehaviour
{
    private SpriteRenderer spiteRenderer;
    [SerializeField] private float maxAlphaValue = 0.35f;

    private void Start()
    {
        spiteRenderer = GetComponent<SpriteRenderer>();
        StartCoroutine(Waiter());
    }
    IEnumerator Waiter()
    {
        while (true)
        {
            int wait_time = Random.Range(2, 4);
            yield return new WaitForSecondsRealtime(wait_time);
            spiteRenderer.DOFade(Random.Range(0, maxAlphaValue), 1f);
        }
    }
}
