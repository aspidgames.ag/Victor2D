using System.Collections;
using UnityEngine;

public class LampShaker : MonoBehaviour
{
    [SerializeField] new private HingeJoint2D hingeJoint;
    private void Start()
    {
        StartCoroutine(Shake());
    }
    IEnumerator Shake()
    {
        while (true)
        {
            hingeJoint.useMotor = true;
            float wait_time = Random.Range(0.5f, 1.5f);
            yield return new WaitForSecondsRealtime(wait_time);
            hingeJoint.useMotor = false;
            yield return new WaitForSecondsRealtime(wait_time);
        }
    }
}
