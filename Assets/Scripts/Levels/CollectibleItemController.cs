using UnityEngine;
using DG.Tweening;

public class CollectibleItemController : MonoBehaviour
{
    [SerializeField] private bool isPuzzle;
    [SerializeField] private string title;

    [SerializeField] private int chapterIndex;
    [SerializeField] private int puzzleIndex;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            if (!isPuzzle)
            {
                InventoryController.AddItemToInvetory(title);
            }
            else
            {
                PuzzlePhotosController.puzzlePhotos[chapterIndex - 1].openedPuzzleIndexes[puzzleIndex - 1] = 1;

                if (InventoryController.GetItemFromPlayerInventory($"Puzzle_Photo_0{chapterIndex}") == null)
                {
                    InventoryController.AddItemToInvetory($"Puzzle_Photo_0{chapterIndex}");
                }
            }
            transform.DOScale(0.01f, 0.25f).OnComplete(() => { Destroy(gameObject); });
        }
    }
}
