using UnityEngine;
using DG.Tweening;

public class SecretRoomController : MonoBehaviour
{
    [SerializeField] private GameObject secretRoomMask;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            SpriteRenderer[] sprites = secretRoomMask.GetComponentsInChildren<SpriteRenderer>();

            foreach (var sprite in sprites)
            {
                sprite.DOFade(0, 0.75f);
            }
        }
    }
}
