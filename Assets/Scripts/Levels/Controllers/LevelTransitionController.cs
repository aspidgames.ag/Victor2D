using System;
using System.Collections.Generic;
using UnityEngine;

public class LevelTransitionController : MonoBehaviour
{
    [SerializeField] private bool isGoToSubLevel;

    [SerializeField] private int stackIndex;
    [SerializeField] private int levelIndex;

    [SerializeField] private int sublevelIndex;

    [SerializeField] EntranceNumber nextEntrance;
    [SerializeField] Direction direction;
    public static EntranceNumber nextEntranceNumber;

    private void Start()
    {
        SetAdaptivePosition();
    }
    private void Update()
    {
        SetAdaptivePosition();
    }
    private void SetAdaptivePosition()
    {
        Vector3 position = Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, Camera.main.pixelHeight, 0));

        if (direction == Direction.Right) transform.position = new Vector3(position.x, transform.position.y, transform.position.z);
        if (direction == Direction.Left) transform.position = new Vector3(-position.x, transform.position.y, transform.position.z);
        if (direction == Direction.Up) transform.position = new Vector3(transform.position.x, position.y, transform.position.z);
        if (direction == Direction.Down) transform.position = new Vector3(transform.position.x, -position.y, transform.position.z);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            if (isGoToSubLevel == false)
            {
                GameController.CreateLevel(stackIndex - 1, levelIndex - 1);
            }
            else
            {
                nextEntranceNumber = nextEntrance;
                GameController.playerController.transform.SetParent(null);
                Debug.Log("Level: " + gameObject.name);
                GameController.currentLevel.gameObject.SetActive(false);
                GameController.currentLevels[sublevelIndex - 1].gameObject.SetActive(true);
                GameController.currentLevels[sublevelIndex - 1].SetStartPosition();
                GameController.currentLevel = GameController.currentLevels[sublevelIndex - 1];
                GameController.OnStartLevelEventHandler?.Invoke();
            }
        }
        else if(collision.tag == "InterlevelObject" || collision.tag == "Movable")
        {
            collision.transform.SetParent(GameController.currentLevels[sublevelIndex - 1].transform);
            collision.transform.position = GameController.currentLevels[sublevelIndex - 1].interLevelObjectStartPos.position;
        }
    }
}
