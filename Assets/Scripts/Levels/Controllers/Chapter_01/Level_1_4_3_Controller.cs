using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level_1_4_3_Controller : MonoBehaviour
{
    [SerializeField] private RotatingPictureController[] pictures;

    public void CheckPicturesScales()
    {
        int correctScalesCount = 0;

        foreach (var picture in pictures)
        {
            if(picture.indexScale == 1)
            {
                correctScalesCount++;
            }
        }

        if(correctScalesCount == pictures.Length)
        {
            foreach (var picture in pictures)
            {
              Destroy(picture);
            }
            Level_1_4_2_Controller.isCanShowMother = true;
        }
    }
}
