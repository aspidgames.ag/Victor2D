using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireplaceItemController : DragAndDropItem
{
    protected override void OnEndDrag()
    {
        base.OnEndDrag();
        if (Level_1_4_6_Controller.fireplace == null)
        {
            transform.position = startPos;
            collider.enabled = true;
        }
        else
        {
            transform.position = Level_1_4_6_Controller.fireplace.transform.position;
            Level_1_4_6_Controller.fireplace.itemsInFirePlase.Add(transform.name);
            Level_1_4_6_Controller.fireplace.FinishLevel();
            Destroy(this);
        }
    }
}
