using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level_1_4_5_TriggerController : MonoBehaviour
{
    [SerializeField] private TriggerType triggerType;

    [SerializeField] private LeverArmController armControllerToNextLevel;


    [SerializeField] private Rigidbody2D platformRigidbody;
    [SerializeField] private SpringJoint2D platformSpringJoint;

    [SerializeField] private float platformMass;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player" && triggerType == TriggerType.ChangePlatformMass)
        {
            if (platformRigidbody != null) platformRigidbody.mass = platformMass;
        }
        else if (triggerType == TriggerType.DisableSpringJoint && collision.tag == "Platform")
        {
            if (platformSpringJoint != null)
            {
                Destroy(platformSpringJoint);
                Destroy(platformRigidbody);
                EnablePlatformToTextLevel();
            }
        }
    }
    private void EnablePlatformToTextLevel()
    {
        armControllerToNextLevel.IsLocked = false;
        armControllerToNextLevel.isNeedToPressButton = true;
        armControllerToNextLevel.platformsToMove[0].distance = 11;
        armControllerToNextLevel.platformsToMove[0].duration = 4.5f;
        armControllerToNextLevel.transformsToRotate[0].duration = 4.5f;
    }
    private enum TriggerType
    {
        ChangePlatformMass,
        DisableSpringJoint
    }
}
