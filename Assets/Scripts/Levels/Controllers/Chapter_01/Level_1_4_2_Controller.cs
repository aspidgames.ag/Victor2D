using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level_1_4_2_Controller : MonoBehaviour
{
    public static bool isCanShowFather;
    public static bool isCanShowMother;
    public static bool isCanShowSon;
    public static bool isCanShowDaughter;

    [SerializeField] private SpriteRenderer father;
    [SerializeField] private SpriteRenderer mother;
    [SerializeField] private SpriteRenderer son;
    [SerializeField] private SpriteRenderer daughter;

    void Awake()
    {
        isCanShowFather = false;
        isCanShowMother = false;
        isCanShowSon = false;
        isCanShowDaughter = false;
    }
    private void OnEnable()
    {
        if(isCanShowFather)
        {
            father.gameObject.SetActive(true);
        }
        if (isCanShowMother)
        {
            mother.gameObject.SetActive(true);
        }
        if (isCanShowSon)
        {
            son.gameObject.SetActive(true);
        }
        if (isCanShowDaughter)
        {
            daughter.gameObject.SetActive(true);
        }
    }
}
