using UnityEngine.EventSystems;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class Level_1_4_6_Controller : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private LeverArmController armController;
    public static Level_1_4_6_Controller fireplace;

    public List<string> itemsInFirePlase = new List<string>();

    [SerializeField] private ParticleSystem[] fires;

    void Start()
    {
        armController.IsLocked = true;
    }
    public void FinishLevel()
    {
        if(itemsInFirePlase.Count == 2)
        {
            StartCoroutine(_FinishLevel());
        }
    }
    private IEnumerator _FinishLevel()
    {
        foreach (var fire in fires)
        {
            fire.Play();
            yield return new WaitForSeconds(0.2f);
        }
        armController.IsLocked = false;
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        fireplace = this;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        fireplace = null;
    }
}
