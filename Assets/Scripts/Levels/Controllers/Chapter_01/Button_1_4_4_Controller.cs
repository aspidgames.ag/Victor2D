using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Button_1_4_4_Controller : MonoBehaviour
{
    [SerializeField] private Level_1_4_4_Controller controller;

    public int indexToEnable;

    [SerializeField] private SpriteRenderer lightRenderer;
    [SerializeField] private SpriteRenderer spotRenderer;

    [SerializeField] private Color lightColor = Color.red;
    private void TurnOnTheLight()
    {
        if(controller.currentButtonIndex == indexToEnable)
        {
            controller.currentButtonIndex++;     
            spotRenderer.DOFade(0.15f, 1f);
            lightRenderer.DOColor(lightColor, 0.15f);

            controller.CheckFinish();
        }
        else
        {
            controller.ResetLevelLights();
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.tag == "Player")
        {
            TurnOnTheLight();
        }
    }
}
