using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level_1_2_1_Controller : MonoBehaviour
{
    [SerializeField] private Rigidbody2D box;
    [SerializeField] private Collider2D coll;

    [SerializeField] private bool isPlatform;
    [SerializeField] private LeverArmController armController;
    void Start()
    {
        if (armController != null)
        {
            armController.IsLocked = true;
        }
    }

    private void OnMouseDown()
    {
        if (box != null)
        {
            box.velocity = Vector3.right;
        }
    }
    private void Update()
    {
        if (!isPlatform) return;

        Debug.DrawRay(transform.position + Vector3.up / 2, Vector2.up * 2, Color.red);
        RaycastHit2D hit = Physics2D.Raycast(transform.position + Vector3.up / 2, Vector2.up, 2f);
        if (hit.collider != null && hit.collider.tag == "Movable")
        {
            armController.IsLocked = false;
        }
        else
        {
            armController.IsLocked = true;
        }
    }
    private void OnDisable()
    {
        if(isPlatform)
        {
            box.transform.parent = transform.parent.parent;
        }
    }


}
