using UnityEngine;
using DG.Tweening;

public class RotatingPictureController : MonoBehaviour
{
    public Level_1_4_3_Controller level_1_4_3_Controller;

    public int indexScale = 1;
    public Direction direction;

    private SpriteRenderer spriteRenderer;
    private bool isCanTurn = true;

    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }
    private void OnMouseDown()
    {
        if (isCanTurn)
        {
            if (direction == Direction.Left)
            {
                transform.DOScaleX(transform.localScale.x * -1, 0.5f).OnComplete(() => { isCanTurn = true; });
            }
            else if (direction == Direction.Up)
            {
                transform.DOScaleY(transform.localScale.y * -1, 0.5f).OnComplete(() => { isCanTurn = true; });
            }
            isCanTurn = false;
            indexScale *= -1;
            level_1_4_3_Controller.CheckPicturesScales();
        }

    }
}
