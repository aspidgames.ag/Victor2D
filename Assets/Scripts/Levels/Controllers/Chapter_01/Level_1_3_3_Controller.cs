using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level_1_3_3_Controller : MonoBehaviour
{
    [SerializeField] private int[] password;
    [SerializeField] private ClockFaceController[] clockFaceControllers;

    public void CheckPassword()
    {
        int count = 0;

        for (int i = 0; i < clockFaceControllers.Length; i++)
        {
            if(clockFaceControllers[i].cellIndex == password[i])
            {
                count++;
            }
        }
        if(count == clockFaceControllers.Length)
        {
            OnMatchPassword();
        }
    }
    private void OnMatchPassword()
    {
        Level_1_3_2_Controller.isCanOpenDoor = true;
        Debug.Log("Password matched");

        for (int i = 0; i < clockFaceControllers.Length; i++)
        {
            clockFaceControllers[i].gameObject.SetActive(false);
        }
    }
}
