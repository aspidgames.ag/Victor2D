using UnityEngine;
using DG.Tweening;

public class Level_1_4_4_Controller : MonoBehaviour
{
    public int currentButtonIndex = 0;

    [SerializeField] private SpriteRenderer[] renderesSpots;
    [SerializeField] private SpriteRenderer[] renderesLights;
    [SerializeField] Button_1_4_4_Controller[] buttons;

    [SerializeField] public Transform platformToMove;

    [SerializeField] private HingeJoint2D[] hingeJoints;

    public void CheckFinish()
    {
        if(currentButtonIndex == buttons.Length)
        {
            foreach (var button in buttons)
            {
                Destroy(button); 
            }
            platformToMove.DOMoveX(platformToMove.position.x +6.5f, 2.5f);

            foreach (var hingeJoint in hingeJoints)
            {
                hingeJoint.enabled = true;
                hingeJoint.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
                Level_1_4_2_Controller.isCanShowFather = true;
            }

            Destroy(this);

        }
    }
    public void ResetLevelLights()
    {
        currentButtonIndex = 0;

        foreach (var renderer in renderesSpots)
        {
            renderer.DOFade(0, 0.15f);
        }
        foreach (var renderer in renderesLights)
        {
            renderer.DOColor(Color.black, 0.15f);
        }
       
    }
}
