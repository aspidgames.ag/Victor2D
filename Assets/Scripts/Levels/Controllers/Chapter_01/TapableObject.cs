using UnityEngine;

public class TapableObject : MonoBehaviour
{
    [SerializeField] private GameObject[] objectsToEnable;
    [SerializeField] private ParticleSystem particleEffect;

    private static int index;

    private void Start()
    {
        index = 0;
    }
    private void OnMouseDown()
    {
        OnTapLogic();
    }
    private void  OnTapLogic()
    {
      
        foreach (var sprite in objectsToEnable)
        {
            sprite.SetActive(false);
        }
        objectsToEnable[index].SetActive(true);
        particleEffect.Play();

       

        if (index == objectsToEnable.Length - 1)
        {
            Destroy(this);
        }
        index++;
    }
}
