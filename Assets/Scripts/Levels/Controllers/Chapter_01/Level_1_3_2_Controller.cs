using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level_1_3_2_Controller : MonoBehaviour
{
    [SerializeField] LeverArmController door;

    public static bool isCanOpenDoor;
    void Start()
    {
        isCanOpenDoor = false;
    }

    private void OnEnable()
    {
        if(isCanOpenDoor)
        {
            door.inventoryItemRequirement = string.Empty;
            door.Activate();

            isCanOpenDoor = false;
        }
    }
}
