using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformController : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Player" || collision.collider.tag == "InterlevelObject")
        {
            collision.transform.SetParent(transform);
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.tag == "Player" || collision.collider.tag == "InterlevelObject")
        {
            collision.transform.SetParent(null);
        }
    }

    private void OnDestroy()
    {
        if (GameController.playerController != null) GameController.playerController.transform.SetParent(null);
    }
}
