using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour
{
    [Header("Level Start Main Position")]

    public Transform playerStartPosition;

    [Header("Level Start Positions")]

    [SerializeField] private Transform Right_Up;
    [SerializeField] private Transform Right_Middle;
    [SerializeField] private Transform Right_Down;
    [SerializeField] private Transform Left_Up;
    [SerializeField] private Transform Left_Middle;
    [SerializeField] private Transform Left_Down;
    [SerializeField] private Transform Up_Left;
    [SerializeField] private Transform Up_Middle;
    [SerializeField] private Transform Up_Right;
    [SerializeField] private Transform Down_Left;
    [SerializeField] private Transform Down_Middle;
    [SerializeField] private Transform Down_Right;

    [SerializeField] private Transform DoorLeft;
    [SerializeField] private Transform DoorRight;
    [SerializeField] private Transform DoorCentral;

    public Transform interLevelObjectStartPos;

    [Header("Hint Text")]

    public string hintText;

    [Header("Level Mechanic Index")]

    public int levelMechanicIndex;

    public void UseHint()
    {
        Debug.Log("Hint was used");
    }
    public void SetStartPosition()
    {
         if (LevelTransitionController.nextEntranceNumber == EntranceNumber.Left_Down && Left_Down != null)
            GameController.playerController.transform.position = Left_Down.position;

        if (LevelTransitionController.nextEntranceNumber == EntranceNumber.Left_Middle && Left_Middle != null)
            GameController.playerController.transform.position = Left_Middle.position;

        if (LevelTransitionController.nextEntranceNumber == EntranceNumber.Left_Up && Left_Up != null)
            GameController.playerController.transform.position = Left_Up.position;

        if (LevelTransitionController.nextEntranceNumber == EntranceNumber.Right_Down && Right_Down != null)
            GameController.playerController.transform.position = Right_Down.position;

        if (LevelTransitionController.nextEntranceNumber == EntranceNumber.Right_Middle && Right_Middle != null)
            GameController.playerController.transform.position = Right_Middle.position;

        if (LevelTransitionController.nextEntranceNumber == EntranceNumber.Right_Up && Right_Up != null)
            GameController.playerController.transform.position = Right_Up.position;

        if (LevelTransitionController.nextEntranceNumber == EntranceNumber.Down_Left && Down_Left != null)
            GameController.playerController.transform.position = Down_Left.position;

        if (LevelTransitionController.nextEntranceNumber == EntranceNumber.Down_Middle && Down_Middle != null)
            GameController.playerController.transform.position = Down_Middle.position;

        if (LevelTransitionController.nextEntranceNumber == EntranceNumber.Down_Right && Down_Right != null)
            GameController.playerController.transform.position = Down_Right.position;

        if (LevelTransitionController.nextEntranceNumber == EntranceNumber.Up_Left && Up_Left != null)
            GameController.playerController.transform.position = Up_Left.position;

        if (LevelTransitionController.nextEntranceNumber == EntranceNumber.Up_Middle && Up_Middle != null)
            GameController.playerController.transform.position = Up_Middle.position;

        if (LevelTransitionController.nextEntranceNumber == EntranceNumber.Up_Right && Up_Right != null)
            GameController.playerController.transform.position = Up_Right.position;

        if (LevelTransitionController.nextEntranceNumber == EntranceNumber.DoorCentral && DoorCentral != null)
            GameController.playerController.transform.position = DoorCentral.position;

        if (LevelTransitionController.nextEntranceNumber == EntranceNumber.DoorLeft && DoorLeft != null)
            GameController.playerController.transform.position = DoorLeft.position;

        if (LevelTransitionController.nextEntranceNumber == EntranceNumber.DoorRight && DoorRight != null)
            GameController.playerController.transform.position = DoorRight.position;
    }
}
