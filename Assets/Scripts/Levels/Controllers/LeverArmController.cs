﻿using System.Collections;
using UnityEngine;
using DG.Tweening;
using EPOOutline;

public class LeverArmController : MonoBehaviour
{
    public Outlinable outlinable { get; private set; }

    public string inventoryItemRequirement;
    [SerializeField] private string inventoryItemToGive;
    [SerializeField] private GameObject[] objectsToEnable;
    [SerializeField] private GameObject[] objectsToDisable;
    [SerializeField] private GameObject[] objectsToEnableOnFinish;
    [SerializeField] private GameObject[] objectsToDisableOnFinish;
    [SerializeField] private Transform[] secretRoomsToOpen;
    [SerializeField] private Transform[] secretRoomsToClose;
    [SerializeField] private Animator[] animators;
    [SerializeField] private ParticleSystem[] particles;
    public TransformToRotate[] transformsToRotate;
    public PlatformToMove[] platformsToMove;
    [SerializeField] private MonoBehaviour[] componentsToEnableOnStart;
    [SerializeField] private MonoBehaviour[] componentsToDisableOnStart;
    [SerializeField] private MonoBehaviour[] componentsToEnableOnFinish;
    [SerializeField] private MonoBehaviour[] componentsToDisableOnFinish;
    [SerializeField] private float timeToShakeCamera = 2f;
    [SerializeField] private float duration = 2f;
    [SerializeField] private int index = -1;
    [SerializeField] private int steps;

    [Header("Text")]

    [SerializeField] private string noKeyText = $"No key item was found in inventory";

    [Header("Level Transition")]

    [SerializeField] private bool isGoToSubLevel;
    [SerializeField] private int stackIndex;
    [SerializeField] private int levelIndex;
    [SerializeField] private int sublevelIndex;
    [SerializeField] EntranceNumber nextEntrance;

    [Header("Bools")]

    public bool isNeedToPressButton = true;
    public bool isNeedToDiasble;
    public bool isNeedToShakeCamera;
    public bool isDisablePlayer;
    public bool isToEnablePlayerOnComplete;
    public bool isResetPositionOnStart;
    public bool isLevelTransition;


    private bool isCanActivate = true;
    public bool IsLocked  {get;set;} 
    private bool isReverse = false;

    private int currentStep;
    private Vector3[] startPos;
    private Quaternion[] startRotations;
    private bool isStarted;

    void Start()
    {
        outlinable = GetComponent<Outlinable>();
        startPos = new Vector3[platformsToMove.Length];
        startRotations = new Quaternion[transformsToRotate.Length];

        for (int i = 0; i < platformsToMove.Length; i++)
        {
            startPos[i] = platformsToMove[i].platform.position;
        }
        for (int i = 0; i < transformsToRotate.Length; i++)
        {
            startRotations[i] = transformsToRotate[i].transform.rotation;
        }
        isStarted = true;
    }
    private void OnEnable()
    {
        if(isResetPositionOnStart && isStarted)
        {
            for (int i = 0; i < platformsToMove.Length; i++)
            {
                platformsToMove[i].platform.position = startPos[i];
            }
            for (int i = 0; i < transformsToRotate.Length; i++)
            {
                transformsToRotate[i].transform.rotation = startRotations[i];
            }
            currentStep = 0;
            IsLocked = false;
            isReverse = false;
        }
    }
    public void ResetPosition()
    {
        isCanActivate = false;

        for (int i = 0; i < platformsToMove.Length; i++)
        {
            platformsToMove[i].platform.DOMove (startPos[i], 1f);
        }
        for (int i = 0; i < transformsToRotate.Length; i++)
        {
            transformsToRotate[i].transform.DORotate(startRotations[i].eulerAngles, 1f).OnComplete (()=> { isCanActivate = true; });
        }
        currentStep = 0;
        IsLocked = false;
        isReverse = false;
    }
    public void Activate()
    {
        if (isCanActivate && IsLocked == false)
        {
            if (inventoryItemRequirement == string.Empty || inventoryItemRequirement == "")
            {
                ActivationLogic();
            }
            else
            {
                if(InventoryController.GetItemFromPlayerInventory(inventoryItemRequirement) != null)
                {
                    ActivationLogic();
                }
                else
                {
                    LevelView.ShowTextEventHandler?.Invoke(noKeyText);
                }
            }
            if (inventoryItemToGive != null || inventoryItemToGive == "")
            {
                InventoryController.AddItemToInvetory(inventoryItemToGive);
            }
        }
    }
    private void ActivationLogic()
    {
        if (index == GameController.currentLevel.levelMechanicIndex)
        {
            Activation();
        }
        else if (index == -1)
        {
            Activation();
            GameController.currentLevel.levelMechanicIndex++;
        }
        else if (index == -2)
        {
            Activation();
        }
    }
    private void Activation()
    {
        if (isLevelTransition)
        {
            LevelMechanics.ChangeLevel(isGoToSubLevel, stackIndex, levelIndex, sublevelIndex, nextEntrance);
            return;
        }
        LevelMechanics.EnableObject(objectsToEnable);

        LevelMechanics.DisableObjecs(objectsToDisable);

        LevelMechanics.PlayAnimators(animators, "Activation");

        LevelMechanics.PlayParticles(particles);

        LevelMechanics.EnableComponentsOnStart(componentsToEnableOnStart);

        LevelMechanics.DisableComponentsStart(componentsToDisableOnStart);

        LevelMechanics.OpenSecretRooms(secretRoomsToOpen);

        LevelMechanics.CloseSecretRooms(secretRoomsToClose);

        RotateObjects();

        MovePlatform();

        if (isReverse == false)
        {
            currentStep++;
        }
        else currentStep--;

        if (currentStep == 0)
        {
            isReverse = false;
        }
        else if (currentStep == steps)
        {
            isReverse = true;
        }
        if(isNeedToShakeCamera)
        {
            CameraShaker.instance.testProperties.duration = timeToShakeCamera;
            CameraShaker.instance.StartShake();
        }
        if(isDisablePlayer)
        {
            GameController.playerController.EnablePlayerController(false);
        }
        if (!isLevelTransition)
        {
            StartCoroutine(OnComplete());
            //isCanActivate = true;
        }
    }
    private IEnumerator OnComplete()
    {
        yield return new WaitForSeconds(duration);

        if (isToEnablePlayerOnComplete)
        {
           GameController.playerController.EnablePlayerController(true);
        }
        if (isNeedToDiasble)
        {
            IsLocked = true;
        }
        LevelMechanics.EnableObject(objectsToEnableOnFinish);

        LevelMechanics.DisableObjecs(objectsToDisableOnFinish);

        LevelMechanics.EnableComponentsOnStart(componentsToEnableOnStart);

        LevelMechanics.DisableComponentsStart(componentsToDisableOnStart);

    }
    private void RotateObjects()
    {
        isCanActivate = false;

        foreach (TransformToRotate transform in transformsToRotate)
        {
            LevelMechanics.RotateObject(transform.transform, transform.degrees, transform.duration, isReverse, () => {
                isCanActivate = true; });
        }
    }
    private void MovePlatform()
    {
        isCanActivate = false;

        foreach (var platform in platformsToMove)
        {
            LevelMechanics.MoveTransfrom(platform.platform, platform.direction, platform.distance, platform.duration, isReverse, () =>{ 
                isCanActivate = true; });
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player" && !isNeedToPressButton)
        {
            Activate();
        }
    }
}

[System.Serializable]
public class PlatformToMove
{
    public Vector3 direction;
    public Transform platform;
    public float distance;
    public float duration;
}

[System.Serializable]
public class TransformToRotate
{
    public Transform transform;
    public float degrees;
    public float duration;
}