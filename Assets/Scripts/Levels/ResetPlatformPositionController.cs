using UnityEngine;

public class ResetPlatformPositionController : MonoBehaviour
{
    [SerializeField] private LeverArmController armController;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            armController.ResetPosition();
        }
    }
}
