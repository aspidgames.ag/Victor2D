﻿using DG.Tweening;
using UnityEngine;
using System;
public static class LevelMechanics
{
    public static void EnableObject(GameObject[] objectsToEnable)
    {
        for (int i = 0; i < objectsToEnable.Length; i++)
        {
            objectsToEnable[i].SetActive(true);
        }
    }
    public static void DisableObjecs(GameObject[] objectsToDisable)
    {
        for (int i = 0; i < objectsToDisable.Length; i++)
        {
            objectsToDisable[i].SetActive(false);
        }
    }
    public static void PlayAnimators(Animator[] animators, string animationName, int layer = 0)
    {
        for (int i = 0; i < animators.Length; i++)
        {
            animators[i].enabled = true;
            animators[i].Play(animationName, layer);
        }
    }
    public static void PlayParticles(ParticleSystem[] particlesSystem)
    {
        for (int i = 0; i < particlesSystem.Length; i++)
        {
            particlesSystem[i].Play();
        }
    }
    public static void EnableComponentsOnStart(MonoBehaviour[] components)
    {
        for (int i = 0; i < components.Length; i++)
        {
            components[i].GetComponent<Rigidbody>().isKinematic = false;
            components[i].GetComponent<Collider2D>().enabled = true;
        }
    }
    public static void DisableComponentsStart(MonoBehaviour[] components)
    {
        for (int i = 0; i < components.Length; i++)
        {
            components[i].GetComponent<Rigidbody>().isKinematic = true;
            components[i].GetComponent<Collider2D>().enabled = false;
        }
    }
    public static void EnableComponentsOnFinish(MonoBehaviour[] components)
    {
        for (int i = 0; i < components.Length; i++)
        {
            components[i].GetComponent<Rigidbody>().isKinematic = false;
            components[i].GetComponent<Collider2D>().enabled = true;
        }
    }
    public static void DisableComponentsOnFinish(MonoBehaviour[] components)
    {
        for (int i = 0; i < components.Length; i++)
        {
            components[i].GetComponent<Rigidbody>().isKinematic = true;
            components[i].GetComponent<Collider2D>().enabled = false;
        }
    }
    public static void OpenSecretRooms(Transform[] rooms)
    {
        foreach (var room in rooms)
        {
            SpriteRenderer[] sprites = room.GetComponentsInChildren<SpriteRenderer>();

            foreach (var sprite in sprites)
            {
                sprite.DOFade(0, 0.75f).OnComplete(() => { room.gameObject.SetActive(false); });
            }
        }
    }
    public static void CloseSecretRooms(Transform[] rooms)
    {
        foreach (var room in rooms)
        {
            room.gameObject.SetActive(true);

            SpriteRenderer[] sprites = room.GetComponentsInChildren<SpriteRenderer>();

            foreach (var sprite in sprites)
            {
                sprite.DOFade(1, 0.75f);
            }
        }
    }
    public static void RotateObject(Transform transformToRotate, float degrees, float duration, bool reverse, Action OnComplete)
    {
        Vector3 currentTransformRotation = transformToRotate.rotation.eulerAngles;
        Vector3 targetTransformRotation;

        if (reverse == false)
        {
            targetTransformRotation = currentTransformRotation + (Vector3.forward * degrees);
        }
        else targetTransformRotation = currentTransformRotation - (Vector3.forward * degrees);

        transformToRotate.DOLocalRotate(targetTransformRotation, duration).OnComplete(() => OnComplete?.Invoke());
    }
    public static void MoveTransfrom(Transform platform, Vector3 direction, float distance, float duration, bool reverse, Action OnComplete)
    {
        if (reverse == false)
        {
            platform.DOLocalMove(platform.localPosition + direction * distance, duration).OnComplete(() => OnComplete?.Invoke());
        }
        else platform.DOMove(platform.localPosition - direction * distance, duration).OnComplete(() => OnComplete?.Invoke()); ;
    }
    public static void ChangeLevel(bool isGoToSubLevel, int stackIndex, int levelIndex, int sublevelIndex, EntranceNumber nextEntrance)
    {
        if (isGoToSubLevel == false)
        {
            GameController.CreateLevel(stackIndex - 1, levelIndex - 1);
        }
        else
        {
            LevelTransitionController.nextEntranceNumber = nextEntrance;
            GameController.playerController.transform.SetParent(null);
            GameController.currentLevel.gameObject.SetActive(false);
            GameController.currentLevels[sublevelIndex - 1].gameObject.SetActive(true);
            GameController.currentLevels[sublevelIndex - 1].SetStartPosition();
            GameController.currentLevel = GameController.currentLevels[sublevelIndex - 1];
            GameController.OnStartLevelEventHandler?.Invoke();
        }
    }
}