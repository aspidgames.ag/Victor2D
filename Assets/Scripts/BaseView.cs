using UnityEngine;

public class BaseView : MonoBehaviour
{
    public MainMenuView MainMenuView { get; private set; }
    public LevelView LevelView { get; private set; }
    public PauseView PauseView { get; private set; }
    public InventoryView InventoryView { get; private set; }
    public ConfirmationView ConfirmationView{ get; private set; }

    private Camera cameraUI;

    protected virtual void Start()
    {
        MainMenuView = Resources.Load<MainMenuView>("Screens/MainMenuView");
        LevelView = Resources.Load<LevelView>("Screens/LevelView");
        PauseView = Resources.Load<PauseView>("Screens/PauseView");
        InventoryView = Resources.Load<InventoryView>("Screens/InventoryView");
        ConfirmationView = Resources.Load<ConfirmationView>("Screens/ConfirmationView");


        cameraUI = GameObject.Find("Camera_UI").GetComponent<Camera>();
        Debug.Log(gameObject.name);
    }

    protected virtual BaseView OpenScreen(ScreenType screenType)
    {
        BaseView view = null;

        if (screenType == ScreenType.MainMenu)
        {
            view = Instantiate(MainMenuView, Vector3.zero, Quaternion.identity);
        }
        else if (screenType == ScreenType.LevelView)
        {
            view = Instantiate(LevelView, Vector3.zero, Quaternion.identity);
        }
        else if (screenType == ScreenType.Pause)
        {
            view = Instantiate(PauseView, Vector3.zero, Quaternion.identity);
        }
        else if (screenType == ScreenType.Confirmation)
        {
            view = Instantiate(ConfirmationView, Vector3.zero, Quaternion.identity);
        }
        else if (screenType == ScreenType.Inventory)
        {
            view = Instantiate(InventoryView, Vector3.zero, Quaternion.identity);
        }
        view.GetComponent<Canvas>().worldCamera = cameraUI;

        return view;
    }
    protected virtual void CloseScreen(ScreenType screenType)
    {
        GameObject screen = null;
        if (screenType == ScreenType.LevelView)
        {
            if (FindObjectOfType<LevelView>())
                screen = FindObjectOfType<LevelView>().gameObject;
        }
        else if (screenType == ScreenType.MainMenu)
        {
            if (FindObjectOfType<MainMenuView>())
                screen = FindObjectOfType<MainMenuView>().gameObject;
        }
        else if (screenType == ScreenType.Pause)
        {
            if (FindObjectOfType<PauseView>())
                screen = FindObjectOfType<PauseView>().gameObject;
        }
        else if (screenType == ScreenType.Confirmation)
        {
            if (FindObjectOfType<ConfirmationView>())
                screen = FindObjectOfType<ConfirmationView>().gameObject;
        }
        else if (screenType == ScreenType.Inventory)
        {
            if (FindObjectOfType<InventoryView>())
                screen = FindObjectOfType<InventoryView>().gameObject;
        }
        if (screen != null)
        {
            Destroy(screen);
        }
    }
}