using UnityEngine.UI;
using UnityEngine;

public class PauseView : BaseView
{
    [Header("Buttons")]

    [SerializeField] private Button resumeButton;
    [SerializeField] private Button homeButton;

    protected override void Start()
    {
        base.Start();

        resumeButton.onClick.AddListener(ResumeButtonPressed);
        homeButton.onClick.AddListener(HomeButtonPressed);

        PauseController.PauseGame(true);
    }
    private void ResumeButtonPressed()
    {
        CloseScreen(ScreenType.Pause);
        PauseController.PauseGame(false);
    }
    private void HomeButtonPressed()
    {
        var screen = OpenScreen(ScreenType.Confirmation);
        screen.GetComponent<ConfirmationView>().confirmButton.onClick.AddListener(GoHome);
    }
    private void GoHome()
    {
        OpenScreen(ScreenType.MainMenu);
        CloseScreen(ScreenType.Pause);
        CloseScreen(ScreenType.LevelView);
        CloseScreen(ScreenType.Confirmation);
        GameController.ClearLevels();
        GameController.playerController.gameObject.SetActive(false);
        PauseController.PauseGame(false);
    }
}
