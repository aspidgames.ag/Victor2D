using UnityEngine;

public class PauseController : MonoBehaviour
{
       public static void PauseGame(bool isPauseOn)
    {
        if(isPauseOn)
        {
            //Time.timeScale = 0;
            GameController.playerController.enabled = false;
        }
        else
        {
            //Time.timeScale = 1;
            GameController.playerController.enabled = true;
        }
    }
}
