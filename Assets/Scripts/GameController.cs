using UnityEngine;
using System.Collections.Generic;
using System;

public class GameController : MonoBehaviour
{
    // Events
    public static Action OnStartLevelEventHandler;

    // Levels
    private static LevelController[] chapter_01_Stack_01;
    private static LevelController[] chapter_01_Stack_02;
    private static LevelController[] chapter_01_Stack_03;
    private static LevelController[] chapter_01_Stack_04;

    private static List<LevelController[]> chapterLevels_01 = new List<LevelController[]>();
    private static List<List<LevelController[]>> levels = new List<List<LevelController[]>>();
    public static List<LevelController> currentLevels = new List<LevelController>();
    public static LevelController currentLevel;

    //PlayerController
    public static PlayerController playerController;

    //Counters
    public static int currentStackIndex;
    public static int currentLevelIndex;

    private void Start()
    {
        InitializeLevels();

        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        playerController.gameObject.SetActive(false);
    }
    private void InitializeLevels()
    {
        chapter_01_Stack_01 = Resources.LoadAll<LevelController>("Levels/Chapter_01/Stack_01");
        chapter_01_Stack_02 = Resources.LoadAll<LevelController>("Levels/Chapter_01/Stack_02");
        chapter_01_Stack_03 = Resources.LoadAll<LevelController>("Levels/Chapter_01/Stack_03");
        chapter_01_Stack_04 = Resources.LoadAll<LevelController>("Levels/Chapter_01/Stack_04");

        chapterLevels_01.Add(chapter_01_Stack_01);
        chapterLevels_01.Add(chapter_01_Stack_02);
        chapterLevels_01.Add(chapter_01_Stack_03);
        chapterLevels_01.Add(chapter_01_Stack_04);

        levels.Add(chapterLevels_01);
    }
    public static void ClearLevels()
    {
        if (currentLevels.Count != 0)
        {
            for (int i = 0; i < currentLevels.Count; i++)
            {
                Destroy(currentLevels[i].gameObject);
            }
        }
        currentLevels.Clear();
    }
    public static void CreateLevel(int stackIndex, int levelIndex)
    {
        ClearLevels();

        foreach (var level in levels[stackIndex][levelIndex])
        {
            LevelController _level = Instantiate(level);
            currentLevels.Add(_level);
        }
        foreach (var level in currentLevels)
        {
            level.gameObject.SetActive(false);
        }
        currentLevels[0].gameObject.SetActive(true);
        currentLevel = currentLevels[0];

        playerController.gameObject.SetActive(true);
        playerController.transform.position = currentLevel.playerStartPosition.position;
        playerController.EnablePlayerController(true);
        currentLevel.playerStartPosition = null;

        OnStartLevelEventHandler?.Invoke();
    }
}
