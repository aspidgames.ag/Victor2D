using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragAndDropItem : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    protected Vector3 startPos;
    protected Vector3 screenPoint;
    protected Vector3 offset;

    protected new Collider2D collider;
    protected new Rigidbody2D rigidbody;

    void Start()
    {
        startPos = transform.position;
        collider = GetComponent<Collider2D>();
        rigidbody = GetComponent<Rigidbody2D>();
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        screenPoint = Camera.main.WorldToScreenPoint(transform.position);
        offset = transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
       if(rigidbody != null) rigidbody.isKinematic = true;
    }
    public void OnDrag(PointerEventData eventData)
    {
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);

        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
        transform.position = curPosition;
        collider.enabled = false;

    }
    public void OnEndDrag(PointerEventData eventData)
    {
        collider.enabled = true;
        if (rigidbody != null) rigidbody.isKinematic = false;
        OnEndDrag();

    }
    protected virtual void OnEndDrag()
    {

    }
}
