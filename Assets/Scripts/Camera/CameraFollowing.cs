﻿using UnityEngine;
public class CameraFollowing : MonoBehaviour
{
    Vector3 velocity = Vector3.zero;
    public float smoothTime = 0.15f;
    private Transform target;
    public bool yMaxEnebled = false;
    public float yMax = 0;
    public bool yMinEnebled = false;
    public float yMin = 0;
    public bool xMaxEnebled = false;
    public float xMax = 0;
    public bool xMinEnebled = false;
    public float xMin = 0;
    private void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
    }
    void FixedUpdate()
    {
        if (target != null)
        {
            Vector3 targetPos = new Vector3(target.position.x, target.position.y, target.position.z - 10);

            if (yMinEnebled && yMaxEnebled)
                targetPos.y = Mathf.Clamp(target.position.y, yMin, yMax);

            else if (yMinEnebled)
                targetPos.y = Mathf.Clamp(targetPos.y, yMin, target.position.y);

            else if (yMaxEnebled)
                targetPos.y = Mathf.Clamp(targetPos.y, targetPos.y, yMax);

            if (xMinEnebled && xMaxEnebled)
                targetPos.x = Mathf.Clamp(target.position.x, xMin, xMax);

            else if (xMinEnebled)
                targetPos.x = Mathf.Clamp(targetPos.x, xMin, target.position.x);

            else if (xMaxEnebled)
                targetPos.x = Mathf.Clamp(targetPos.x, targetPos.x, xMax);

            transform.position = Vector3.SmoothDamp(transform.position, targetPos, ref velocity, smoothTime);
        }
    }
}