﻿using System.Collections.Generic;
using UnityEngine;

public class InventoryController : MonoBehaviour
{
    public static InventroyItemController[] inventoryItems;
    public static List<InventroyItemController> currentPlayerItems = new List<InventroyItemController>();

    private void Start()
    {
        inventoryItems = Resources.LoadAll<InventroyItemController>("InventoryItems");
    }
    public static void AddItemToInvetory(string title)
    {
        if(GetItem(title) != null) currentPlayerItems.Add(GetItem(title));
    }
    public static void RemoveItemFromInventory(string title)
    {
        if (GetItemFromPlayerInventory(title) != null) currentPlayerItems.Remove(GetItemFromPlayerInventory(title));
    }
    private static InventroyItemController GetItem(string title)
    {
        InventroyItemController inventroyItem = null;
        foreach (var item in inventoryItems)
        {
            if(item.inventroyItemModel.title == title)
            {
                inventroyItem = item;
                break;
            }
        }
        return inventroyItem;
    }
    public static InventroyItemController GetItemFromPlayerInventory(string title)
    {
        InventroyItemController inventroyItem = null;
        foreach (var item in currentPlayerItems)
        {
            if (item.inventroyItemModel.title == title)
            {
                inventroyItem = item;
                break;
            }
        }
        return inventroyItem;
    }
}
