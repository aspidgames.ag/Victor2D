﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class InventoryView : BaseView
{
    [Header("Transforms")]

    [SerializeField] private Transform inventoryItemsParent;
    [SerializeField] private Transform inventoryCamera;

    [Header("Buttons")]

    [SerializeField] private Button closeButton;
    [SerializeField] private Button moveLeftButton;
    [SerializeField] private Button moveRightButton;

    private float moveTime = 0.5f;
    private float moveStep = 1000;
    private int currentItemIndex = 0;

    protected override void Start()
    {
        base.Start();

        closeButton.onClick.AddListener(CloseButtonPressed);
        moveLeftButton.onClick.AddListener(MoveLeftButtonPressed);
        moveRightButton.onClick.AddListener(MoveRightButtonPressed);
        PauseController.PauseGame(true);

        InitializeInvetory();
    }
    private void InitializeInvetory()
    {
        for (int i = 0; i < InventoryController.currentPlayerItems.Count; i++)
        {
            var item = Instantiate(InventoryController.currentPlayerItems[i]);
            item.transform.DOScale(1, 0f);
            item.transform.SetParent(inventoryItemsParent);
            item.transform.localPosition = new Vector3(i * moveStep, 0);
 
            item.transform.DOScale(item.inventroyItemModel.size, 1f);
        }
    }
    private void MoveLeftButtonPressed()
    {
        if (currentItemIndex > 0)
        {
            currentItemIndex--;
            inventoryCamera.DOLocalMoveX(moveStep * currentItemIndex, moveTime);
        }
    }
    private void MoveRightButtonPressed()
    {
        if (currentItemIndex < InventoryController.currentPlayerItems.Count - 1)
        {
            currentItemIndex++;
            inventoryCamera.DOLocalMoveX(moveStep * currentItemIndex, moveTime);
        }
    }
    private void CloseButtonPressed()
    {
        OpenScreen(ScreenType.LevelView);
        PauseController.PauseGame(false);
        CloseScreen(ScreenType.Inventory);
    }
}
