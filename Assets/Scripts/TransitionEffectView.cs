using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;

public class TransitionEffectView : BaseView
{
    [SerializeField] private Image fadeImage;
    protected override void Start()
    {
        GameController.OnStartLevelEventHandler += FadeEffect;
    }
    private void FadeEffect()
    {
        fadeImage.gameObject.SetActive(true);
        fadeImage.DOFade(0, 0f);
        fadeImage.DOFade(1, 0f).OnComplete(() =>
        {
            fadeImage.DOFade(0, 0.8f).OnComplete(() =>
            {
                fadeImage.gameObject.SetActive(false);
            });

        });
    }
}
