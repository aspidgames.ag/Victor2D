[System.Serializable]
public class PuzzleModel
{
    public int chapter;
    public int[] openedPuzzleIndexes;

    public PuzzleModel(int chapter, int[] openedPuzzleIndexes)
    {
        this.chapter = chapter;
        this.openedPuzzleIndexes = openedPuzzleIndexes;
    }
}
