using UnityEngine;

public class PuzzlePhotoView : MonoBehaviour
{
    [Header("Chapter")]
    [SerializeField] private int chapterIndex = 1;

    [Header("Materials")]
    [SerializeField] private Material puzzleNormalMat;

    [Header("Puzzles")]
    [SerializeField] private MeshRenderer[] puzzles;

    private void OnEnable()
    {
        CheckPuzzle();
    }
    public void CheckPuzzle()
    {
        for (int i = 0; i < puzzles.Length; i++)
        {
            if(PuzzlePhotosController.puzzlePhotos[chapterIndex - 1].openedPuzzleIndexes[i] == 1)
            {
                puzzles[i].material = puzzleNormalMat;
            }
        }
    }
}
