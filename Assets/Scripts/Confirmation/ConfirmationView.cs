using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ConfirmationView : BaseView
{
    public Button confirmButton;
    public Button declineButton;

    [Header("Texts")]

    public TextMeshProUGUI descriptionText;

    protected override void Start()
    {
        base.Start();
        declineButton.onClick.AddListener(CloseScreen);
    }
    public void ResetButtons()
    {
        confirmButton.onClick.RemoveAllListeners();
        declineButton.onClick.RemoveAllListeners();
    }
    private void CloseScreen()
    {
        CloseScreen(ScreenType.Confirmation);
    }
    private void OnDestroy()
    {
        ResetButtons();
    }
}
