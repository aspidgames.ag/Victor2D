using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class MainMenuView : BaseView
{
    [Header("Tabs")]
    [SerializeField] private GameObject mainMenu;
    [SerializeField] private GameObject stack_01;

    [Header("Buttons")]
    [SerializeField] private Button settingsButton;
    [SerializeField] private Button rateUsButton;
    [SerializeField] private Button noAdsButton;
    [SerializeField] private Button stack_01_Button;
    [SerializeField] private Button[] stack_01_Buttons;
    [SerializeField] private Button moveLeftButton;
    [SerializeField] private Button moveRightButton;
    [SerializeField] private Button backButton;

    [Header("RectTransforms")]
    [SerializeField] private RectTransform[] stacksButtons;
    [SerializeField] private RectTransform[] stacks_01_LevelTabs;

    private int currentStackIndex;
    private int currentLevelTabsIndex;

    private float screensMovingDuration = 0.5f;
    private float outScreenPosition = 2000f;

    protected override void Start()
    {
        base.Start();

        moveLeftButton.onClick.AddListener(delegate { MoveTabs(true, stacksButtons, ref currentStackIndex); });
        moveRightButton.onClick.AddListener(delegate { MoveTabs(false, stacksButtons, ref currentStackIndex); });
        stack_01_Button.onClick.AddListener(delegate { StackButtonPressed(stack_01, stacks_01_LevelTabs); });
        backButton.onClick.AddListener(BackButtonsPressed);

        InitializeLevelsButtons(ref stack_01_Buttons, 0);
    }

    private void MoveTabs(bool isLeft, RectTransform[] tabs, ref int index)
    {
        if (isLeft)
        {
            if (index > 0)
            {
                tabs[index].DOLocalMoveX(outScreenPosition, screensMovingDuration);
                tabs[index - 1].DOLocalMoveX(-outScreenPosition, 0);
                tabs[index - 1].DOLocalMoveX(0, screensMovingDuration);
                index--;
            }
        }
        else
        {
            if (index >= 0 && index < tabs.Length - 1)
            {
                tabs[index].DOLocalMoveX(-outScreenPosition, screensMovingDuration);
                tabs[index + 1].DOLocalMoveX(outScreenPosition, 0);
                tabs[index + 1].DOLocalMoveX(0, screensMovingDuration);
                index++;
            }
        }
    }
    private void StackButtonPressed(GameObject stackTab, RectTransform[] levelTabs)
    {
        mainMenu.SetActive(false);
        stackTab.SetActive(true);
        currentLevelTabsIndex = 0;

        for (int i = 0; i < levelTabs.Length; i++)
        {
            levelTabs[i].DOLocalMoveX(2000, 0);
        }
        levelTabs[0].DOLocalMoveX(0, 0);

        moveLeftButton.onClick.RemoveAllListeners();
        moveRightButton.onClick.RemoveAllListeners();
        moveLeftButton.onClick.AddListener(delegate { MoveTabs(true, levelTabs, ref currentLevelTabsIndex); });
        moveRightButton.onClick.AddListener(delegate { MoveTabs(false, levelTabs, ref currentLevelTabsIndex); });
    }
  
    private void BackButtonsPressed()
    {
        mainMenu.SetActive(true);
        stack_01.SetActive(false);

        moveLeftButton.onClick.RemoveAllListeners();
        moveRightButton.onClick.RemoveAllListeners();
        moveLeftButton.onClick.AddListener(delegate { MoveTabs(true, stacksButtons, ref currentStackIndex); });
        moveRightButton.onClick.AddListener(delegate { MoveTabs(false, stacksButtons, ref currentStackIndex); });
    }
    private void LevelButtonPressed(int stackIndex, int levelindex)
    {
        GameController.CreateLevel(stackIndex, levelindex);
        CloseScreen(ScreenType.MainMenu);
        OpenScreen(ScreenType.LevelView);
    }
    private void InitializeLevelsButtons(ref Button[] levelButtons, int stackIndex)
    {
        for (int i = 0; i < levelButtons.Length; i++)
        {
            int index = i;
            levelButtons[i].onClick.AddListener(delegate { LevelButtonPressed(stackIndex, index); });
        }
    }
}
