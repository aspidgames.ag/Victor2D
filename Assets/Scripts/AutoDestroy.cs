using System.Collections;

using UnityEngine;
using Lean.Pool;

public class AutoDestroy : MonoBehaviour
{
    [SerializeField] private float despawnDelay;
    void OnEnable()
    {
        StartCoroutine(Despawn());
    }
    IEnumerator Despawn()
    {
        yield return new WaitForSeconds(despawnDelay);

        LeanPool.Despawn(gameObject);
    }
}
