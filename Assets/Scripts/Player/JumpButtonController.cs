using UnityEngine.EventSystems;
using UnityEngine;
public class JumpButtonController : MonoBehaviour, IPointerDownHandler
{
    public void OnPointerDown(PointerEventData eventData)
    {
      if(GameController.playerController.isControllerEnabled) GameController.playerController.MobileJump();
    }
}
