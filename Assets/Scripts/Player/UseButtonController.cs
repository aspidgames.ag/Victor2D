using UnityEngine.EventSystems;
using UnityEngine;

public class UseButtonController : MonoBehaviour, IPointerDownHandler
{
    public void OnPointerDown(PointerEventData eventData)
    {
        if (GameController.playerController.isControllerEnabled) GameController.playerController.ActivateLeverArm();
    }
}
