using UnityEngine.EventSystems;
using UnityEngine;

public class MovementButtonController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField] private int movementDirection;
    public void OnPointerDown(PointerEventData eventData)
    {
        if (GameController.playerController.isControllerEnabled) GameController.playerController.horizontalInput = movementDirection;
    }
    public void OnPointerUp(PointerEventData eventData)
    {
        GameController.playerController.horizontalInput = 0;
    }
}
