﻿using UnityEngine;
using Lean.Pool;
using TMPro;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float runSpeed;
    public float RunSpeed { get { return runSpeed; } set { runSpeed = value; } }

    [SerializeField] private float jumpVelocity;
    public float JumpVelocity { get { return jumpVelocity; } set { jumpVelocity = value; } }
    public Direction charachterDirection { get; private set; }
    public bool isGrounded { get; private set; }
    public bool isControllerEnabled { get; private set; }

    [SerializeField] private LayerMask layerMask;
    [SerializeField] private float rayDistance;
    [SerializeField] private float horizontalRayDistance = 1f;
    [SerializeField] private float horizontalRayOffset;
    [SerializeField] private float activationRadius = 1f;

    [Header("Animators")]
    [SerializeField] private Animator[] animators;

    [Header("VFX")]
    [SerializeField] private ParticleSystem jumpDustEffect;

    [Header("VFX Positions")]
    [SerializeField] private Transform jumpDustEffectPosition;



    public float horizontalInput { get; set; }
    private Rigidbody2D rb;
    private LeverArmController currentLeverArm;
    private RaycastHit2D downCentralHit;
    private RaycastHit2D downLeftHit;
    private RaycastHit2D downRightHit;
    private RaycastHit2D leftHit;
    private RaycastHit2D rightHit;

    public bool isEditorController;

    public static bool isNearLeverArm;

    private bool isJustGrounded;
    private bool isMovingObject;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();

        GameController.playerController = this;
    }
    void Update()
    {
        PlayerMovement();
        PlayerAnimator();
        PlayerJump();
        CheckLeverArm();
        Raycasts();

        if (Input.GetKeyDown(KeyCode.E))
        {
            ActivateLeverArm();
        }
    }
    public void EnablePlayerController(bool enabled)
    {
        isControllerEnabled = enabled;
    }
    void PlayerMovement()
    {
        if (isControllerEnabled == false) return;

        if (isEditorController) horizontalInput = Input.GetAxisRaw("Horizontal");

        rb.velocity = new Vector3(horizontalInput * RunSpeed, rb.velocity.y);

        if (horizontalInput < 0) charachterDirection = Direction.Left;

        else if (horizontalInput > 0) charachterDirection = Direction.Right;
    }
    private void PlayerAnimator()
    {
        foreach (var anim in animators)
        {
            anim.SetFloat("xValue", horizontalInput);

            if (horizontalInput != 0 && isGrounded && isMovingObject)
            {
                anim.SetLayerWeight(1, 0);
                anim.SetLayerWeight(2, 0);
                anim.SetLayerWeight(3, 0);
                anim.SetLayerWeight(4, 1);
            }
            else  if (horizontalInput == 0 && isGrounded)
            {
                anim.SetLayerWeight(1, 1);
                anim.SetLayerWeight(2, 0);
                anim.SetLayerWeight(3, 0);
                anim.SetLayerWeight(4, 0);

                if (isJustGrounded == false && downCentralHit)
                    PlayDustEffect();
            }
            else if (horizontalInput != 0 && isGrounded && !isMovingObject)
            {
                anim.SetLayerWeight(1, 0);
                anim.SetLayerWeight(2, 1);
                anim.SetLayerWeight(3, 0);
                anim.SetLayerWeight(4, 0);

                if (isJustGrounded == false && downCentralHit)
                    PlayDustEffect();
            }
            else if (!isGrounded)
            {
                anim.SetLayerWeight(1, 0);
                anim.SetLayerWeight(2, 0);
                anim.SetLayerWeight(3, 1);
                anim.SetLayerWeight(4, 0);

                isJustGrounded = false;
            }
           
        }
    }
    private void PlayDustEffect()
    {
        var jumpEffect = LeanPool.Spawn(jumpDustEffect);
        jumpEffect.transform.position = jumpDustEffectPosition.position;
        isJustGrounded = true;
    }

    private void Raycasts()
    {
        downCentralHit = Physics2D.Raycast(transform.position, Vector3.down, rayDistance, layerMask);
        downLeftHit = Physics2D.Raycast(transform.position + Vector3.left * horizontalRayOffset, Vector3.down, rayDistance, layerMask);
        downRightHit = Physics2D.Raycast(transform.position + Vector3.right * horizontalRayOffset, Vector3.down, rayDistance, layerMask);

        leftHit = Physics2D.Raycast(transform.position, Vector3.left, horizontalRayDistance, layerMask);
        rightHit = Physics2D.Raycast(transform.position, Vector3.right, horizontalRayDistance, layerMask);

        Debug.DrawRay(transform.position, Vector3.down * rayDistance, Color.red);
        Debug.DrawRay(transform.position + Vector3.left * horizontalRayOffset, Vector3.down * rayDistance, Color.red);
        Debug.DrawRay(transform.position + Vector3.right * horizontalRayOffset, Vector3.down * rayDistance, Color.red);

        Debug.DrawRay(transform.position, Vector3.left * horizontalRayDistance, Color.red);
        Debug.DrawRay(transform.position, Vector3.right * horizontalRayDistance, Color.red);

        if (leftHit.collider != null && leftHit.collider.tag == "Movable")
        {
            isMovingObject = true;           
        }
        else if (rightHit.collider != null && rightHit.collider.tag == "Movable")
        {
            isMovingObject = true;
        }
        else
        {
            isMovingObject = false;
        }
    }
    public void PlayerJump()
    {
        if (downCentralHit.collider || downLeftHit || downRightHit)
        {
            isGrounded = true;
        }
        else isGrounded = false;

        if (Input.GetKeyDown(KeyCode.Space) && isGrounded == true)
        {
            if (isControllerEnabled == false) return;

            rb.velocity = new Vector2(rb.velocity.x, JumpVelocity);
            PlayDustEffect();
        }

        
        //transform.rotation = Quaternion.FromToRotation(transform.up, centralHit.normal) * transform.rotation;
    }
    public void MobileJump()
    {
        if (isGrounded == true)
        {
            rb.velocity = new Vector2(rb.velocity.x, JumpVelocity);
        }
    }
    public void ActivateLeverArm()
    {
        if (currentLeverArm != null)
        {
            currentLeverArm.Activate();
        }
    }
    public void CheckLeverArm()
    {
        Collider2D[] colls = Physics2D.OverlapCircleAll(transform.position, activationRadius);

        foreach (var coll in colls)
        {
            if (coll.tag == "LeverArm")
            {
                currentLeverArm = coll.GetComponent<LeverArmController>();

                if (currentLeverArm != null) currentLeverArm.outlinable.enabled = true;

                isNearLeverArm = true;
                break;
            }
            else
            {
                if (currentLeverArm != null) currentLeverArm.outlinable.enabled = false;

                currentLeverArm = null;
                isNearLeverArm = false;
            }
        }
    }
}
